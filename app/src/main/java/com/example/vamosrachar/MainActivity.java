package com.example.vamosrachar;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.text.DecimalFormat;

public class MainActivity extends AppCompatActivity implements TextToSpeech.OnInitListener {
    ImageButton btShare, btVoice;
    EditText edPerson, edPrice;
    TextView tvTotal;
    TextWatcher mTextWatcher;
    TextToSpeech ttsPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tvTotal = (TextView) findViewById(R.id.txtResult);
        btShare = (ImageButton) findViewById(R.id.shareButton);
        btShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_TEXT, "O valor para pagar é: "+tvTotal.getText().toString());
                startActivity(intent);
            }
        });
        edPrice =  ( EditText ) findViewById(R.id.editTextPrice);
        edPerson =  ( EditText ) findViewById(R.id.editTextPerson);
        mTextWatcher = new TextWatcher() {
            @Override public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }
            @Override
            public void afterTextChanged(Editable editable) {

                String value = edPrice.getText().toString();
                String value2 = edPerson.getText().toString();
                try {
                    float finalValue = Float.parseFloat(value);
                    float finalValue2 = Float.parseFloat(value2);

                    float result = calcular(finalValue, finalValue2);

                    DecimalFormat df = new DecimalFormat("0.00");
                    String resultFormat = df.format(result);
                    tvTotal.setText(""+resultFormat);

                }
                catch(Exception e){
                    tvTotal.setText("0,00");
                }
            }
        };
        edPerson.addTextChangedListener(mTextWatcher);
        edPrice.addTextChangedListener(mTextWatcher);

        Intent checkTTSIntent = new Intent();
        checkTTSIntent.setAction(TextToSpeech.Engine.ACTION_CHECK_TTS_DATA);
        startActivityForResult(checkTTSIntent, 1122);
        btVoice = (ImageButton) findViewById(R.id.btVoice);
        btVoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ttsPlayer!=null){
                    ttsPlayer.speak("O valor para pagar é: "+tvTotal.getText().toString()+"reais", TextToSpeech.QUEUE_FLUSH, null, "ID1");
                }
            }
        });
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode ==  1122){
            if (resultCode == TextToSpeech.Engine.CHECK_VOICE_DATA_PASS){
                ttsPlayer = new TextToSpeech(this, this);
            }
            else{
                Intent installTTSIntent = new Intent();
                installTTSIntent.setAction(TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
                startActivity(installTTSIntent);
            }
        }
    }
    @Override
    public void onInit(int status) {
        if(status == TextToSpeech.SUCCESS){
            Toast.makeText(this, "TTS ativado...", Toast.LENGTH_LONG).show();
        }else if(status == TextToSpeech.ERROR){
            Toast.makeText(this, "Sem TTS habilitado", Toast.LENGTH_LONG).show();
        }
    }

    public float calcular(float a, float b){
        return a/b;
    }


}